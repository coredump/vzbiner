# VZBot Toolhead board

This is a toolhead board for the [VZBot 3D printer][1], using the aluminum printhead and the CNC version of the VZ-Hextrudort extruder.

This is **not a CAN board**. It's based on the [Carabiner series][10] by Annex Engineering, but **NOT COMPATIBLE**. You can use the same harness/cable used by the Carabiner series, but do not use this board with anything else 

## Pinout differences

If a pin isn't listed on the table below, the function is the same as the [Carabiner pinout][12]

|Pin|Carabiner|This board|Function|
|--|--|--|--|
|1|Chamber thermistor|X endstop|Self explanatory
|2|Part cooling fan Ground|Filament sensor|For sensors that use a microswitch, won't work with overly complicated sensors or anything that needs more than 1 pin and a GND
|7|Aux 1|Probe GND|See below
|8|Aux 3|Probe VCC|See below
|11|Alt voltage|Common GND|GND for the endstop and filament sensor
|15|Aux 2|Probed D+|See below
|16|Aux 3|Probe D-|See below

Mainly, unlike the Carabiner, this board doesn't include:

- Pads for thermistors or diodes in case of inductive probes, or support for any probe that can't be used using 4 or less wires
- Part fans connectors (use CPAP)
- Alternate voltage (Everything has to use the hot end voltage)

### Aux pins

Carabiner has Aux1 to Aux4 on pins 7,8,15,16. This board has the same connector, but it's designed as a USB compatible connector for probes like the [Beacon][11]. The official cable instructions from annex also use a twisted pair cable on pins 15,16 that are good for the `D+` and `D-` wires in USB. 

In case the beacon is not being used, the pins are free to be used anyway. 

The traces for the heater are large enough to use up to 5A heaters. The fan and stepper traces should be good up to 2A. 

(Images may be out of date with the actual board, so always check the kicad files)

![Example assemble][2]

![Render][3]

## BOM

This assumes that two boards will be assembled, one on the toolhead, and the other as a distributor near the printer electronics.

|Item|Quantity|Why|Link|
|--  |--      |-- |--  |
|PCB|2| | |
|Molex 043045-1600|2|Right Angle connector|[Digikey][4]|
|Molex 043025-1600|2|16 pin mating connector|[Digikey][5]
|Molex 043650-0200|2|Heater power connector|[Digikey][6]
|Molex 043645-0200|2|Mating heater power connector|[Digikey][7]
|JST XH 2 pin |8| Thermistors, endstop, and fan headers | |
|JST XH 4 pin |4| Stepper and probe headers | |
|22mm M3 Standoff|2|To secure the board to the stepper| |

The Standoff can be anywhere from 21 to 22 milimiters. Can also be 3d printed. Model/Drone stores usually have aluminum standoffs/spacers on those weird sizes.

## Procuring

The [Gerber file][8] for the PCB can be uploaded to one of the PCB making services around and should always be current.

## Altering

Feel free to alter the board in any way. This is a Kicad 7 project, and also included are [schematics in PDF][9].

## License stuff

Copyright Jose Eufrasio 2023.                                                    
                                                                             
This source describes Open Hardware and is licensed under the CERN-OHL-S v2. 
                                                                             
You may redistribute and modify this source and make products using it under 
the terms of the CERN-OHL-S v2 (https://ohwr.org/cern_ohl_s_v2.txt).         
                                                                             
This source is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,          
INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A         
PARTICULAR PURPOSE. Please see the CERN-OHL-S v2 for applicable conditions.  
                                                                             
Source location: https://gitlab.com/coredump/vz-printhead-board
                                                                             
As per CERN-OHL-S v2 section 4, should You produce hardware based on this    
source, You must where practicable maintain the Source Location visible      
on the external case of the vzbot toolhead board or other products you make using this      
source.                                                                      

[1]: https://github.com/VzBoT3D/VzBoT-Vz330
[2]: images/image1.png
[3]: images/image2.png
[4]: https://www.digikey.com/en/products/detail/molex/0430451600/531424
[5]: https://www.digikey.com/en/products/detail/molex/0430251600/531406
[6]: https://www.digikey.com/en/products/detail/molex/0436500200/268989
[7]: https://www.digikey.com/en/products/detail/molex/0436450200/268974
[8]: gerber.zip
[9]: vz-printhead-board.pdf
[10]: https://github.com/Annex-Engineering/Annex_Engineering_PCBs/tree/master/carabiner-series-toolboard
[11]: https://beacon3d.com/
[12]: https://github.com/Annex-Engineering/Annex_Engineering_PCBs/tree/master/carabiner-series-toolboard/carabiner#wiring-guide